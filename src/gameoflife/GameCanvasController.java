package gameoflife;

import gameoflife.GameGrid.CellState;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * Controller of the FXML Canvas visualizing the Game Of Life grid of
 * MainWindow.
 */
public class GameCanvasController implements Initializable
{
    private GameOfLifeHandler gameHandler; // reference to main handler

    private Point2D mousePressCoords;

    // for storing and subsequently moving a selected figure
    private CellFigure selectedFigure;
    // index at which selected figure is currently grabbed
    private int selectedFigureGrabIndex;

    private boolean showCellLiveIndicators;

    @FXML private Pane gameCanvasPane;
    @FXML private Canvas gameCanvas;

    // for visualizing indicators whether cell will live
    private static final double INDICATOR_WILLLIVE_SIZE = 5; // 1/x of cell size
    private static final double INDICATOR_CENTER_OFFSET = 0.5 - 1 / (2 * INDICATOR_WILLLIVE_SIZE);

    // Colors
    private static final Color COLOR_STROKE = Color.BLACK;
    private static final Color COLOR_CELLALIVE = Color.BLUE;
    private static final Color COLOR_INDICATOR_WILLLIVE = Color.GREEN;
    private static final Color COLOR_SELECTEDFIGURE = Color.CADETBLUE;
    
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        // Handle Resize
        gameCanvas.widthProperty().bind(gameCanvasPane.widthProperty());
        gameCanvas.heightProperty().bind(gameCanvasPane.heightProperty());
        gameCanvas.widthProperty().addListener((event) -> updateGameCanvas());
        gameCanvas.heightProperty().addListener((event) -> updateGameCanvas());
    }

    public void setGameHandlerReference(final GameOfLifeHandler gameHandler) {
        this.gameHandler = gameHandler;
    }

    public void reset() {
        selectedFigure = new CellFigure(0, 0);
        selectedFigureGrabIndex = -1;
    }

    public boolean getShowCellLiveIndicators() {
        return showCellLiveIndicators;
    }

    public void setShowCellLiveIndicators(final boolean showIndicators) {
        this.showCellLiveIndicators = showIndicators;
    }

    /**
     * update currently selected figure if other window sends figure to this window.
     * 
     * @param figure figure to receive
     */
    public void receiveCellFigure(final CellFigure figure) {
        selectedFigure = new CellFigure(gameHandler.getGridColumns(), gameHandler.getGridRows());

        // store received figure in selectedFigure
        // convert indices to correct grid indices
        for (Integer cellIndex : figure) {
            final int colPos = cellIndex % figure.getColumnSpan();
            final int rowPos = cellIndex / figure.getColumnSpan();
            selectedFigure.add(rowPos * gameHandler.getGridColumns() + colPos);
        }

        selectedFigureGrabIndex = 0;
        updateGameCanvas();
    }

    /**
     * redraws the game canvas.
     */
    public void updateGameCanvas() {
        // put drawing in runLater block
        // otherwise sometimes InternalErrors are triggered (java.lang.InternalError:
        // Unrecognized PGCanvas token)
        javafx.application.Platform.runLater(() -> {
            final GraphicsContext gfxContext = gameCanvas.getGraphicsContext2D();
            gfxContext.setStroke(COLOR_STROKE);
            gfxContext.clearRect(0, 0, gameCanvas.getWidth(), gameCanvas.getHeight());

            final int cols = gameHandler.getGridColumns();
            final int rows = gameHandler.getGridRows();
            final double colSize = gameCanvas.getWidth() / cols;
            final double rowSize = gameCanvas.getHeight() / rows;
            // // ensure cells are square
            // colSize = Math.min(colSize, rowSize);
            // rowSize = colSize;

            // draw columns
            for (int i = 0; i <= cols; ++i) {
                gfxContext.strokeLine(i * colSize, 0, i * colSize, rows * rowSize);
            }

            // draw rows
            for (int i = 0; i <= rows; ++i) {
                gfxContext.strokeLine(0, i * rowSize, cols * colSize, i * rowSize);
            }

            // draw alive cells
            gfxContext.setFill(COLOR_CELLALIVE);
            for (int cellIndex = 0; cellIndex < gameHandler.getGridSize(); ++cellIndex) {
                if (gameHandler.isCellAliveNow(cellIndex)) {
                    gfxContext.fillRect((cellIndex % cols) * colSize, (cellIndex / cols) * rowSize,
                                        colSize, rowSize);
                }
            }

            // draw figure
            if (selectedFigureGrabIndex >= 0) {
                gfxContext.setFill(COLOR_SELECTEDFIGURE);
                for (Integer cellIndex : selectedFigure) {
                    gfxContext.fillRect((cellIndex % cols) * colSize, (cellIndex / cols) * rowSize,
                                        colSize, rowSize);
                }
            }

            // draw "will be alive" indicators
            if (showCellLiveIndicators) {
                gfxContext.setFill(COLOR_INDICATOR_WILLLIVE);
                for (int cellIndex = 0; cellIndex < gameHandler.getGridSize(); ++cellIndex) {
                    if (gameHandler.isCellAliveNext(cellIndex)) {
                        gfxContext.fillOval((cellIndex % cols + INDICATOR_CENTER_OFFSET) * colSize,
                                            (cellIndex / cols + INDICATOR_CENTER_OFFSET) * rowSize,
                                            colSize / INDICATOR_WILLLIVE_SIZE,
                                            rowSize / INDICATOR_WILLLIVE_SIZE);
                    }
                }
            }
        });
    }

    // -------------------------------------------------------------------------
    // UI Interaction/Events
    // -------------------------------------------------------------------------

    /**
     * selects figure if MiddleMouseButton press. stores mouse coordinates to not
     * overwrite MouseDrag handler in MouseRelease handler
     * 
     * @param event MouseEvent
     */
    public void onMousePress(final MouseEvent event) {
        mousePressCoords = new Point2D(event.getX(), event.getY());

        if (event.isMiddleButtonDown() && selectedFigureGrabIndex < 0) {
            final int cellIndex = getCellIndexByMousePos(event.getX(), event.getY());
            if (gameHandler.isCellAliveNow(cellIndex)) {
                selectedFigureGrabIndex = cellIndex;
                selectedFigure = gameHandler.getConnectedCells(cellIndex);
                gameHandler.setCellStates(selectedFigure, CellState.DEAD);
                updateGameCanvas();
            }
        }
    }

    /**
     * switch cell state if clicked. store moved figure in grid
     * 
     * @param event MouseEvent
     */
    public void onMouseRelease(final MouseEvent event) {
        final Point2D newPos = new Point2D(event.getX(), event.getY());
        // assert press and release is at same position, otherwise event is handled in
        // mouse drag
        if (mousePressCoords.equals(newPos)) {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                final int cellIndex = getCellIndexByMousePos(event.getX(), event.getY());
                gameHandler.setCellState(cellIndex, CellState.ALIVE);
            } else if (event.getButton().equals(MouseButton.SECONDARY)) {
                final int cellIndex = getCellIndexByMousePos(event.getX(), event.getY());
                gameHandler.setCellState(cellIndex, CellState.DEAD);
            }
            updateGameCanvas();
        }
        // if figure is selected, modify grid and clear selection
        if (selectedFigureGrabIndex >= 0) {
            gameHandler.setCellStates(selectedFigure, CellState.ALIVE);
            selectedFigure.clear();
            selectedFigureGrabIndex = -1;
            updateGameCanvas();
        }
    }

    /**
     * switch cell states if dragged over.
     * 
     * @param event MouseEvent
     */
    public void onMouseDrag(final MouseEvent event) {
        if (event.isPrimaryButtonDown()) { // revive cells
            final int cellIndex = getCellIndexByMousePos(event.getX(), event.getY());
            if (!gameHandler.isCellAliveNow(cellIndex)) {
                gameHandler.setCellState(cellIndex, CellState.ALIVE);
                updateGameCanvas();
            }
        } else if (event.isSecondaryButtonDown()) { // kill cells
            final int cellIndex = getCellIndexByMousePos(event.getX(), event.getY());
            if (gameHandler.isCellAliveNow(cellIndex)) {
                gameHandler.setCellState(cellIndex, CellState.DEAD);
                updateGameCanvas();
            }
        } else if (event.isMiddleButtonDown()) { // move figure
            if (selectedFigureGrabIndex >= 0) {
                final int curIndex = getCellIndexByMousePos(event.getX(), event.getY());
                final int indexShift = curIndex - selectedFigureGrabIndex;
                if (indexShift != 0) {
                    gameHandler.moveFigure(selectedFigure, indexShift);
                    selectedFigureGrabIndex = curIndex;
                    updateGameCanvas();
                }
            }
        }
    }

    /**
     * returns index of cell selected my mouse coordinates.
     * @param mouseX mouse x-coordinate
     * @param mouseY mouse y-coordinate
     * @return index of selected cell
     */
    public int getCellIndexByMousePos(final double mouseX, final double mouseY) {
        final int cols = gameHandler.getGridColumns();
        final int rows = gameHandler.getGridRows();

        final double colSize = gameCanvas.getWidth() / cols;
        final double rowSize = gameCanvas.getHeight() / rows;

        // ensure indices fit in grid (because of fringe cases on canvas border pixels)
        final int posX = Math.max(0, Math.min((int) (mouseX / colSize), cols - 1));
        final int posY = Math.max(0, Math.min((int) (mouseY / rowSize), rows - 1));

        return posY * cols + posX;
    }
}
