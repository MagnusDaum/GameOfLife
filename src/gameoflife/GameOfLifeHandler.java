package gameoflife;

import gameoflife.GameGrid.CellState;

import java.util.ArrayList;
import java.util.List;


/**
 * This is the main class regarding the implementation of game of life.
 * implements the 2D grid including all cells and their interaction rule set
 */
public class GameOfLifeHandler
{
    private final GameGrid gameGrid;

    protected int numSteps;
    protected int numAliveCells;

    /**
     * Constructor.
     * @param columns columns of grid
     * @param rows rows of grid
     */
    public GameOfLifeHandler(final int columns, final int rows) {
        gameGrid = new GameGrid(columns, rows);
        numSteps = 0;
        numAliveCells = 0;
    }

    // ------------------------------------------------------------
    // Getters
    // ------------------------------------------------------------

    public int getGridSize() {
        return gameGrid.getGridSize();
    }

    public int getGridColumns() {
        return gameGrid.getGridColumns();
    }

    public int getGridRows() {
        return gameGrid.getGridRows();
    }

    public int getStepCount() {
        return numSteps;
    }
    
    public int getAliveCellCount() {
        return numAliveCells;
    }

    public boolean isCellAliveNow(final int cellIndex) {
        return gameGrid.isCellAlive(cellIndex);
    }
    
    public CellFigure getConnectedCells(final int cellIndex) {
        return gameGrid.getConnectedCells(cellIndex);
    }
    
    public GameGrid getGrid() {
        return gameGrid;
    }

    // ------------------------------------------------------------
    // Cell Setters
    // ------------------------------------------------------------

    /**
     * sets the state of a single cell.
     * @param cellIndex which cell to set
     * @param state new state of cell
     */
    public void setCellState(final int cellIndex, final CellState state) {
        // only set if state is different to current cell state
        if (gameGrid.getCellState(cellIndex) != state) {
            gameGrid.setCellState(cellIndex, state);
            if (state == CellState.ALIVE) {
                ++numAliveCells;
            } else {
                --numAliveCells;
            }
        }
    }

    /**
     * sets the state of all given cells.
     * @param figure cells to set
     * @param state new state of given cells
     */
    public void setCellStates(final CellFigure figure, final CellState state) {
        for (Integer cellIndex : figure) {
            setCellState(cellIndex, state);
        }
    }

    /**
     * switches the state of all cells of the grid.
     */
    public void invertGridCells() {
        for (int cellIndex = 0; cellIndex < gameGrid.getGridSize(); ++cellIndex) {
            CellState nextState = gameGrid.getCellState(cellIndex).next();
            setCellState(cellIndex, nextState);
        }
    }

    /**
     * moves figure on the grid.
     * @param figure which cells to move
     * @param indexShift how much should cells be moved
     */
    public void moveFigure(final CellFigure figure, final int indexShift) {
        if (indexShift == 0) {
            return;
        }

        final int gridColumns = gameGrid.getGridColumns();
        final int gridRows = gameGrid.getGridRows();
        final CellFigure shiftedFigure = new CellFigure(gridColumns, gridRows);

        for (Integer cellIndex : figure) {
            int newIndex = cellIndex + indexShift;

            // check if index is valid
            // moving through right border
            if (cellIndex % gridColumns + indexShift % gridColumns >= gridColumns) {
                newIndex -= gridColumns;
            }
            // moving through left border
            if (cellIndex % gridColumns < -indexShift % gridColumns) {
                newIndex += gridColumns;
            }
            // moving trough top/bottom border
            newIndex = Math.floorMod(newIndex, gameGrid.getGridSize());

            shiftedFigure.add(newIndex);
        }

        // add new indices back to figure
        // figure = shiftedFigure is not possible, as it would only affect the local
        // figure
        figure.clear();
        figure.addAll(shiftedFigure);
    }

    // ------------------------------------------------------------
    // Functions for Game Of Life cycling
    // ------------------------------------------------------------

    /**
     * implements rule set of Game of Life.
     * @param cellIndex index of selected cell of grid
     * @return true if cell is alive in next step
     */
    public boolean isCellAliveNext(final int cellIndex) {
        final int livingNeighbors = gameGrid.getAliveNeighborCount(cellIndex);

        final boolean isAlive = isCellAliveNow(cellIndex);

        return isAlive && (livingNeighbors == 2 || livingNeighbors == 3) 
            || !isAlive && livingNeighbors == 3;
    }

    /**
     * applies Game Of Life rules to perform single step.
     */
    public void doStep() {
        // update cells on grid copy
        final List<CellState> nextGrid = new ArrayList<CellState>(gameGrid.getGridSize());

        numAliveCells = 0;

        // apply game of life rules
        for (int cellIndex = 0; cellIndex < gameGrid.getGridSize(); ++cellIndex) {
            if (isCellAliveNext(cellIndex)) {
                nextGrid.add(cellIndex, CellState.ALIVE);
                ++numAliveCells;
            } else {
                nextGrid.add(cellIndex, CellState.DEAD);
            }
        }

        // transfer to actual grid
        for (int cellIndex = 0; cellIndex < gameGrid.getGridSize(); ++cellIndex) {
            gameGrid.setCellState(cellIndex, nextGrid.get(cellIndex));
        }

        ++numSteps;
    }
}