package gameoflife;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;

/**
 * Controller for window showing selection of cell figures.
 * Those figures can be inserted into the grid of the main window.
 * Don't forget to set up reference to main window via setMainWindow()
 */
public class FigureWindowController implements Initializable
{
    private static final int DOUBLE_CLICK_COUNT = 2;
    
    private MainWindowController mainWindow;
    private GameFileIo gameFileIo;
    private GraphicsContext gfxContext; // for drawing on canvas

    private Stage figureStage;
    @FXML private ListView<String> figureListView;
    @FXML private Pane canvasPane;
    @FXML private Canvas canvas;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        
        // initialize figure save/loader
        try {
            gameFileIo = new GameFileIo();
        } catch (TransformerConfigurationException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
        
        // Handle Resize
        canvas.widthProperty().bind(canvasPane.widthProperty());
        canvas.heightProperty().bind(canvasPane.heightProperty());
        canvas.widthProperty().addListener((event) -> drawFigure());
        canvas.heightProperty().addListener((event) -> drawFigure());

        gfxContext = canvas.getGraphicsContext2D();
        gfxContext.setStroke(Color.BLACK);
        gfxContext.setFill(Color.BLUE);

        updateFigureListView();
        
        // Handle changed Figure Choice
        figureListView.getSelectionModel().selectedIndexProperty().addListener(
            (obs, oldVal, newVal) -> drawFigure());
        figureListView.getSelectionModel().select(0);
    }

    // For communication with main window
    
    public void setMainWindow(final MainWindowController mainWindow) {
        this.mainWindow = mainWindow;
    }
    
    public void setStage(final Stage stage) {
        this.figureStage = stage;
    }
    
    public Stage getStage() {
        return figureStage;
    }
    
    /**
     * fill listView of preset figures.
     */
    public void updateFigureListView() {
        figureListView.getItems().clear();
        // fill with hard-coded presets
        for (final CellFigurePreset pt : CellFigurePreset.values()) {
            figureListView.getItems().add(pt.toString());
        }
        // fill from files
        final List<String> presetNames = GameFileIo.getAllPresetFigureFilenames();
        figureListView.getItems().addAll(presetNames);
    }
    
    // UI Events

    @FXML
    private void confirmBtnClick() {
        final CellFigure figure = getFigureFromListView();
        mainWindow.receiveCellFigure(figure);
    }

    @FXML
    private void onListViewClick(final MouseEvent event) {
        // check for double click
        if (event.getClickCount() == DOUBLE_CLICK_COUNT) {
            final CellFigure figure = getFigureFromListView();
            mainWindow.receiveCellFigure(figure);
        }
    }

    private CellFigure getFigureFromListView() {
        CellFigure figure;
        final int idx = figureListView.getSelectionModel().selectedIndexProperty().get();
        if (idx >= 0) {
            if (idx < CellFigurePreset.values().length) {
                figure = CellFigurePreset.values()[idx].getFigure();
            } else {
                // read figure from file
                figure = gameFileIo.readFigureFromFile(figureListView.getItems().get(idx));
            }
        } else {
            figure = new CellFigure(0, 0);
        }
        return figure;
    }

    private void drawFigure() {
        final CellFigure figure = getFigureFromListView();

        gfxContext.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

        final int cols = figure.getColumnSpan();
        final int rows = figure.getRowSpan();

        // ensure cells are square
        final double colSize = Math.min(canvas.getWidth() / cols, canvas.getHeight() / rows);
        final double rowSize = colSize;
        
        final double offsetX = (canvas.getWidth() - cols * colSize) / 2.0;
        final double offsetY = (canvas.getHeight() - rows * rowSize) / 2.0;

        // draw figure
        for (Integer cellIndex : figure) {
            gfxContext.fillRect(offsetX + (cellIndex % cols) * colSize,
                                offsetY + (cellIndex / cols) * rowSize,
                                colSize, rowSize);
        }

        // draw columns
        for (int i = 0; i <= cols; ++i) {
            gfxContext.strokeLine(offsetX + i * colSize, offsetY, offsetX + i * colSize,
                                  offsetY + rows * rowSize);
        }

        // draw rows
        for (int i = 0; i <= rows; ++i) {
            gfxContext.strokeLine(offsetX, offsetY + i * rowSize, offsetX + cols * colSize,
                                  offsetY + i * rowSize);
        }
    }

}
