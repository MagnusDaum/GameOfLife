package gameoflife;

import gameoflife.GameGrid.CellState;

// Extends the GameGrid class for some mini game functionality
public class MiniGameHandler extends GameOfLifeHandler
{
    private static final int columns = 50;
    private static final int rows = 50;

    private static final int maxTurns = 100;
    private static final int maxDraws = 200;

    private int score;
    private int drawsLeft;

    /**
     * Constructor.
     */
    public MiniGameHandler() {
        super(columns, rows);
        score = 0;
        drawsLeft = maxDraws;
    }

    public int getScore() {
        return score;
    }

    public int getDrawsLeft() {
        return drawsLeft;
    }

    public int getTurnsLeft() {
        return maxTurns - numSteps;
    }

    /**
     * @see gameoflife.GameOfLifeHandler#doStep()
     */
    @Override
    public void doStep() {
        if (numSteps < maxTurns) {
            super.doStep();
            score += numAliveCells;
        }
    }

    /**
     * @see gameoflife.GameOfLifeHandler#setCellState(int, gameoflife.GameGrid.CellState)
     */
    @Override
    public void setCellState(final int cellIndex, final CellState state) {
        if (drawsLeft > 0) {
            super.setCellState(cellIndex, state);
            --drawsLeft;
        }
    }

}
