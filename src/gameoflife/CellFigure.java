package gameoflife;

import java.util.HashSet;

/**
 * Java has no equivalent of C++ typedef. -> emulate via inheritance
 * additionally store column and row span of figure
 * this is necessary to convert figure indices to grid indices
 */
@SuppressWarnings("serial")
public class CellFigure extends HashSet<Integer>
{
    // These parameters are necessary, to know where figure indices are relative to
    // each other
    // Set stores indices of figure as indices of superordinate grid
    // -> columns/rows of grid are necessary to know how many columns/rows figure
    // has
    // for example:
    // figure indices are 1 and 4. if columnSpan is 3, they're adjacent, otherwise
    // they're not
    private final int columnSpan;
    private final int rowSpan;

    /**
     * Constructor.
     * @param columnSpan necessary to associate figure indices with each other
     * @param rowSpan necessary to associate figure indices with each other
     */
    public CellFigure(final int columnSpan, final int rowSpan) {
        super();
        this.columnSpan = columnSpan;
        this.rowSpan = rowSpan;
    }

    public int getColumnSpan() {
        return columnSpan;
    }

    public int getRowSpan() {
        return rowSpan;
    }
}
