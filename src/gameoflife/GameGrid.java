package gameoflife;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * This is the base class regarding the implementation of game of life.
 * implements the 2D grid including all cells
 */
public class GameGrid
{
    public enum CellState {
        ALIVE, DEAD;
        
        // for switching states (if more states are added in the future)
        private static final CellState[] vals = values();
        public CellState next()
        {
            return vals[(this.ordinal() + 1) % vals.length];
        }
    }
    
    /**
     * for tetragons, the number of adjacent cells is always 8.
     */
    private static final int ADJACENCY_COUNT = 8;
    
    /**
     * this fields represents the cells and holds the state of each.
     * a cell is nothing more than a CellState at a certain index of the grid
     * 2D grid is mapped to a 1D List
     */
    private List<CellState> cells;
    private final int gridColumns;
    private final int gridRows;
    
    /**
     * store adjacent cell indices for each cell in separate list.
     * -> simple lookup during runtime instead of recalculating each cycle
     */
    private int[][] cellAdjacencies;
    
    /**
     * Constructor.
     * @param columns columns of grid
     * @param rows rows of grid
     */
    public GameGrid(final int columns, final int rows) {
        gridColumns = columns;
        gridRows = rows;
        initializeGrid();
    }
    
    public void reset()
    {
        initializeGrid();
    }
    
    /**
     * initializes new grid, all cells are dead.
     */
    private void initializeGrid() {
        final int gridSize = gridColumns * gridRows;
        cells = new ArrayList<CellState>(gridSize);
        for (int i = 0; i < gridSize; ++i) {
            cells.add(CellState.DEAD);
        }

        buildAdjacencyIndices();
    }
    
    // ------------------------------------------------------------
    // Cell Getters
    // ------------------------------------------------------------

    public int getGridColumns() {
        return gridColumns;
    }
    
    public int getGridRows() {
        return gridRows;
    }

    public int getGridSize() {
        return cells.size();
    }
    
    public boolean isCellAlive(final int cellIndex) {
        return cells.get(cellIndex) == CellState.ALIVE;
    }
    
    public CellState getCellState(final int cellIndex) {
        return cells.get(cellIndex);
    }

    /**
     * stores a set of all indices of a connected figure.
     * @param cellIndex start index for finding adjacent cells
     * @return all connected cells
     */
    public CellFigure getConnectedCells(final int cellIndex) {
        final CellFigure connectedCells = new CellFigure(gridColumns, gridRows);
        final Queue<Integer> leftToCheckCells = new LinkedList<Integer>();

        if (cellIndex < 0 || cellIndex >= cells.size()) {
            return connectedCells;
        }
        
        connectedCells.add(cellIndex);
        leftToCheckCells.add(cellIndex);

        while (!leftToCheckCells.isEmpty()) {
            final int curIndex = leftToCheckCells.poll();

            // traverse trough all adjacent cells
            for (int i = 0; i < ADJACENCY_COUNT; ++i) {
                // if they are alive and have not been added yet
                // add and mark for checking as well
                final int neighborIndex = cellAdjacencies[curIndex][i];
                if (isCellAlive(neighborIndex) && !connectedCells.contains(neighborIndex)) {
                    connectedCells.add(neighborIndex);
                    leftToCheckCells.add(neighborIndex);
                }
            }
        }
        return connectedCells;
    }
    
    // ------------------------------------------------------------
    // Cell Setters
    // ------------------------------------------------------------

    /**
     * sets the state of a single cell.
     * @param cellIndex which cell to set
     * @param state new state of cell
     */
    public void setCellState(final int cellIndex, final CellState state) {
        cells.set(cellIndex, state);
    }
    
    
    // ------------------------------------------------------------
    // Adjacencies
    // ------------------------------------------------------------

    /**
     * calculates cell indices of neighbors for each cell.
     * stores those in field cellAdjacencies
     */
    private void buildAdjacencyIndices() {
        final int gridSize = cells.size();
        
        cellAdjacencies = new int[gridSize][ADJACENCY_COUNT];        

        // Establish indices of adjacent cells
        // On borders, those indices have to wrap around the grid
        for (int cellIndex = 0; cellIndex < gridSize; ++cellIndex) {            
            // index of top neighbor
            cellAdjacencies[cellIndex][0] = Math.floorMod(cellIndex - gridColumns, gridSize);
            
            // index of bottom neighbor
            cellAdjacencies[cellIndex][1] = (cellIndex + gridColumns) % gridSize;
            
            int topLeftIndex = cellAdjacencies[cellIndex][0] - 1;
            if (cellIndex % gridColumns == 0) {
                // cellIndex is in leftmost column
                topLeftIndex += gridColumns;
            }
            
            int topRightIndex = cellAdjacencies[cellIndex][0] + 1;
            if (cellIndex % gridColumns == gridColumns - 1) {
                // cellIndex is in rightmost column
                topRightIndex -= gridColumns;
            }

            for (int i = 0; i < 3; ++i) {
                cellAdjacencies[cellIndex][2 + i] = 
                    (topLeftIndex + i * gridColumns) % gridSize; // left
                cellAdjacencies[cellIndex][5 + i] = 
                    (topRightIndex + i * gridColumns) % gridSize; // right
            }
        }
    }
    
    /**
     * counts all adjacent alive cells.
     * @param cellIndex index of cell
     * @return number of adjacent alive cells
     */
    public int getAliveNeighborCount(final int cellIndex) {
        int numAliveNeighbors = 0;

        // traverse trough all adjacent cells
        for (int i = 0; i < ADJACENCY_COUNT; ++i) {
            if (isCellAlive(cellAdjacencies[cellIndex][i])) {
                ++numAliveNeighbors;
            }
        }

        return numAliveNeighbors;
    }
}
