package gameoflife;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

// Class to load/save GameGrid from/to XML file
public class GameFileIo
{    
    public static final String FIGURES_PATH = "Figures\\";
    public static final String SCENARIO_PATH = "Scenarios\\";
    public static final String FILE_EXT_NAME = "xml";
    public static final String FILE_EXT = "." + FILE_EXT_NAME;

    private static final String xmlElemNameRoot = "grid";
    private static final String xmlElementNameCols = "columns";
    private static final String xmlElementNameRows = "rows";
    private static final String xmlElementNameCells = "alive_cells";
    private static final String xmlElementNameCell = "cell";

    private DocumentBuilder docBuilder;
    private Transformer transformer;

    /**
     * Constructor.
     * If at least one exception is thrown, this class is not correctly set up
     * Subsequent calls may not work correctly
     * @throws IOException .
     * @throws ParserConfigurationException .
     * @throws TransformerConfigurationException .
     */
    public GameFileIo() 
        throws ParserConfigurationException, TransformerConfigurationException, IOException {
        
        Files.createDirectories(Paths.get(SCENARIO_PATH));
        Files.createDirectories(Paths.get(FIGURES_PATH));

        final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setIgnoringElementContentWhitespace(true);

        docBuilder = docFactory.newDocumentBuilder();

        final TransformerFactory transformerFac = TransformerFactory.newInstance();
        transformer = transformerFac.newTransformer();
        // These two lines ensure that xml file is properly indented
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
    }
    
    // ------------------------------------------------------------------------
    // Read
    // ------------------------------------------------------------------------

    /**
     * read xml file and create according figure.
     * @param fileName name of xml file
     * @return loaded figure
     */
    public CellFigure readFigureFromFile(final String fileName) {
        final File xmlFile = new File(FIGURES_PATH + fileName + FILE_EXT);
        return readFromFile(xmlFile);
    }
    
    /**
     * read xml file and create according gameGrid.
     * @param fileName name of xml file
     * @return loaded scenario
     */
    public GameOfLifeHandler readScenarioFromFile(final String fileName) {
        final File xmlFile = new File(SCENARIO_PATH + fileName + FILE_EXT);
        final CellFigure figure = readFromFile(xmlFile);

        final GameOfLifeHandler gh = 
            new GameOfLifeHandler(figure.getColumnSpan(), figure.getRowSpan());
        gh.setCellStates(figure, GameGrid.CellState.ALIVE);

        return gh;
    }

    /**
     * read xml file and create according figure.
     * @param xmlFile xml file to load from
     * @return loaded CellFigure
     */
    private CellFigure readFromFile(final File xmlFile) {
        CellFigure figure = new CellFigure(0,0);
        if (xmlFile.exists() && !xmlFile.isDirectory()) {
            try {
                final Document doc = docBuilder.parse(xmlFile);
                doc.getDocumentElement().normalize();
    
                // read column node
                NodeList nodeList = doc.getElementsByTagName(xmlElementNameCols);
                final int cols = Integer.parseInt(nodeList.item(0).getTextContent());
    
                // read row node
                nodeList = doc.getElementsByTagName(xmlElementNameRows);
                final int rows = Integer.parseInt(nodeList.item(0).getTextContent());
    
                // find cells node and read all alive cells
                nodeList = doc.getElementsByTagName(xmlElementNameCells);
                final Element cellsElement = (Element) nodeList.item(0);
                final NodeList aliveCellNodes = 
                    cellsElement.getElementsByTagName(xmlElementNameCell);
                
                // create figure
                figure = new CellFigure(cols, rows);
                for (int i = 0; i < aliveCellNodes.getLength(); ++i) {
                    final int cellIndex = Integer.parseInt(aliveCellNodes.item(i).getTextContent());
                    figure.add(cellIndex);
                }
            } catch (IOException | SAXException e) {
                e.printStackTrace();
            }
        }

        return figure;
    }
    
    // ------------------------------------------------------------------------
    // Write
    // ------------------------------------------------------------------------

    /**
     * create xml document from grid. store as fileName.xml
     * @param fileName name of created file
     * @param gameGrid grid to save
     * @param asFigure indicate if grid is stored as a figure or a scenario
     */
    public boolean writeToFile(final String fileName, final GameGrid gameGrid, boolean asFigure) {
        final DOMSource source = generateDomSource(convertGridToFigure(gameGrid, asFigure));
        
        final String filePath;
        if (asFigure) {
            filePath = FIGURES_PATH + fileName + FILE_EXT;
        } else {
            filePath = SCENARIO_PATH + fileName + FILE_EXT;
        }
        
        final StreamResult streamRes = new StreamResult(new File(filePath));
        
        try {
            transformer.transform(source, streamRes);
        } catch (TransformerException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * create xml DOM source from figure.
     * @param figure given cell figure
     * @return corresponding DOMsource
     */
    private DOMSource generateDomSource(final CellFigure figure) {        
        final Document doc = docBuilder.newDocument();
        final Element gridEl = doc.createElement(xmlElemNameRoot);
        doc.appendChild(gridEl);

        // store column
        final Element colsEl = doc.createElement(xmlElementNameCols);
        colsEl.appendChild(doc.createTextNode(String.valueOf(figure.getColumnSpan())));
        gridEl.appendChild(colsEl);

        // store row
        final Element rowsEl = doc.createElement(xmlElementNameRows);
        rowsEl.appendChild(doc.createTextNode(String.valueOf(figure.getRowSpan())));
        gridEl.appendChild(rowsEl);

        // store alive cells
        final Element cellsEl = doc.createElement(xmlElementNameCells);
        gridEl.appendChild(cellsEl);

        for (Integer cellIndex : figure) {
            final Element cell = doc.createElement(xmlElementNameCell);
            cell.appendChild(doc.createTextNode(cellIndex.toString()));
            cellsEl.appendChild(cell);
        }

        return new DOMSource(doc);
    }
    
    /**.
     * store all alive cells as a single figure
     * necessary for code re-usability
     * @param scaleDown specifies if empty rows/columns should be omitted
     *                      -> indices have to be modified
     * @return all alive cells of grid stored as a figure
     */
    private CellFigure convertGridToFigure(final GameGrid grid, final boolean scaleDown) {
        final int gridColumns = grid.getGridColumns();
        final int gridRows = grid.getGridRows();
        final int gridSize = grid.getGridSize();
        
        CellFigure figure;
        if (scaleDown) {
            // get boundaries of figure
            int minCol = gridColumns;
            int maxCol = 0;
            int minRow = gridRows;
            int maxRow = 0;

            for (int cellIndex = 0; cellIndex < gridSize; ++cellIndex) {
                if (grid.isCellAlive(cellIndex)) {
                    final int col = cellIndex % gridColumns;
                    minCol = Math.min(col, minCol);
                    maxCol = Math.max(col, maxCol);

                    final int row = cellIndex / gridColumns;
                    minRow = Math.min(row, minRow);
                    maxRow = Math.max(row, maxRow);
                }
            }

            // convert grid indices to boundary indices
            figure = new CellFigure(maxCol - minCol + 1, maxRow - minRow + 1);
            for (int cellIndex = 0; cellIndex < gridSize; ++cellIndex) {
                if (grid.isCellAlive(cellIndex)) {
                    final int colPos = cellIndex % gridColumns - minCol;
                    final int rowPos = cellIndex / gridColumns - minRow;
                    figure.add(rowPos * figure.getColumnSpan() + colPos);
                }
            }
        } else {
            // simply store indices in figure structure
            figure = new CellFigure(gridColumns, gridRows);
            for (int cellIndex = 0; cellIndex < gridSize; ++cellIndex) {
                if (grid.isCellAlive(cellIndex)) {
                    figure.add(cellIndex);
                }
            }
        }
        return figure;
    }
    
    // ------------------------------------------------------------------------
    // File Utility
    // ------------------------------------------------------------------------

    // find all xml files in figure folder
    public static List<String> getAllPresetFigureFilenames() {
        return getAllFilenames(FIGURES_PATH);
    }

    // find all xml files in scenario folder
    public static List<String> getAllScenarioFilenames() {
        return getAllFilenames(SCENARIO_PATH);
    }

    // find all xml files in folder
    // return file names
    private static List<String> getAllFilenames(final String folderPath) {
        final List<String> fileNames = new ArrayList<String>();

        final File[] listOfFiles = new File(folderPath).listFiles();
        if (listOfFiles != null) {       
            for (final File file : listOfFiles) {
                String name = file.getName();
                if (file.isFile() && name.endsWith(FILE_EXT)) {
                    // store name without extension
                    name = name.substring(0, name.lastIndexOf(FILE_EXT));
                    fileNames.add(name);
                }
            }
        }

        return fileNames;
    }

}
