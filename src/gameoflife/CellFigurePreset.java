package gameoflife;

// Hardcoded indices to provide certain figures
// returns indices of alive cells necessary to form a certain figure.
public enum CellFigurePreset {

    GLIDER {        
        @Override
        public CellFigure getFigure() {
            final int gridCols = 3;
            final int gridRows = 3;
            
            CellFigure figure = new CellFigure(gridCols, gridRows);

            figure.add(1);
            figure.add(gridCols + 2);
            figure.add(gridCols * 2);
            figure.add(gridCols * 2 + 1);
            figure.add(gridCols * 2 + 2);
            
            return figure;
        }
    },
    
    TEN_ROW {
        @Override
        public CellFigure getFigure() {
            final int gridCols = 10;
            final int gridRows = 1;

            CellFigure figure = new CellFigure(gridCols, gridRows);

            for (int i = 0; i < gridCols; ++i) {
                figure.add(i);
            }

            return figure;
        }
    },
    
    TUMBLER {
        @Override
        public CellFigure getFigure() {
            final int gridCols = 7;
            final int gridRows = 6;

            CellFigure figure = new CellFigure(gridCols, gridRows);

            figure.add(1);

            for (int i = 0; i < 2; ++i) {
                figure.add(1 + i * gridCols);
                figure.add(2 + i * gridCols);
                figure.add(4 + i * gridCols);
                figure.add(5 + i * gridCols);
            }

            figure.add(2 + 2 * gridCols);
            figure.add(4 + 2 * gridCols);

            for (int i = 3; i < 5; ++i) {
                figure.add(0 + i * gridCols);
                figure.add(2 + i * gridCols);
                figure.add(4 + i * gridCols);
                figure.add(6 + i * gridCols);
            }

            figure.add(0 + 5 * gridCols);
            figure.add(1 + 5 * gridCols);
            figure.add(5 + 5 * gridCols);
            figure.add(6 + 5 * gridCols);

            return figure;
        }
    },
    
    EXPLODER {
        @Override
        public CellFigure getFigure() {
            final int gridCols = 5;
            final int gridRows = 5;

            CellFigure figure = new CellFigure(gridCols, gridRows);

            figure.add(2);
            figure.add(2 + (gridRows - 1) * gridCols);

            for (int i = 0; i < gridRows; ++i) {
                figure.add(i * gridCols);
                figure.add((gridCols - 1) + i * gridCols);
            }

            return figure;
        }
    },
    
    SPACESHIP {
        @Override
        public CellFigure getFigure() {
            final int gridCols = 5;
            final int gridRows = 4;

            CellFigure figure = new CellFigure(gridCols, gridRows);

            for (int i = 1; i < gridCols; ++i) {
                figure.add(i);
            }

            figure.add(gridCols);
            figure.add(gridCols + 4);
            figure.add(gridCols * 2 + 4);
            figure.add(gridCols * 3 + 3);

            return figure;
        }
    }, 
    
    HERSCHEL {
        @Override
        public CellFigure getFigure() {
            final int gridCols = 3;
            final int gridRows = 4;

            CellFigure figure = new CellFigure(gridCols, gridRows);

            figure.add(0);

            for (int i = 0; i < gridCols; ++i) {
                figure.add(i + gridCols);
            }

            figure.add(2 * gridCols);
            figure.add(2 * gridCols + 2);
            figure.add(3 * gridCols + 2);

            return figure;
        }
    },
    
    EATER {
        @Override
        public CellFigure getFigure() {
            final int gridCols = 4;
            final int gridRows = 4;

            CellFigure figure = new CellFigure(gridCols, gridRows);

            figure.add(0);
            figure.add(1);
            figure.add(gridCols);
            figure.add(2 + gridCols);
            figure.add(2 + gridCols * 2);
            figure.add(2 + gridCols * 3);
            figure.add(3 + gridCols * 3);

            return figure;
        }
    },
    
    QUEEN_BEE {
        @Override
        public CellFigure getFigure() {
            final int gridCols = 5;
            final int gridRows = 7;

            CellFigure figure = new CellFigure(gridCols, gridRows);

            figure.add(0);
            figure.add(gridCols);
            figure.add(gridCols * 5);
            figure.add(gridCols * 6);
            
            figure.add(1 + gridCols * 2);
            figure.add(1 + gridCols * 3);
            figure.add(1 + gridCols * 4);
            
            figure.add(2 + gridCols * 1);
            figure.add(2 + gridCols * 5);
            
            figure.add(3 + gridCols * 2);
            figure.add(3 + gridCols * 4);
            
            figure.add(4 + gridCols * 3);

            return figure;
        }
    };
    
    public abstract CellFigure getFigure();
}

