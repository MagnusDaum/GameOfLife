package gameoflife;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class SettingsIo
{
    private static final String FILENAME = "settings.cfg";
    private static final String DESCRIPTION = "Settings of Game Of Life Application";
    
    public static final String KEY_LANGUAGE = "language";
    public static final String KEY_WIDTH = "width";
    public static final String KEY_HEIGHT = "height";
    public static final String KEY_OPACITY = "opacity";
    
    private Properties appSettings = new Properties();
    
    /**
     * Constructor.
     */
    public SettingsIo() {
        if (!Files.exists(Paths.get(FILENAME))) {
            writeToFile();
        }
        try {
            appSettings.load(new FileInputStream(FILENAME));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * store properties in settings file.
     */
    public void writeToFile() {
        try {
            appSettings.store(new FileOutputStream(FILENAME), DESCRIPTION);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public String getProperty(String key) {
        return appSettings.getProperty(key);
    }
    
    /**.
     * @param key property key
     * @return property as double
     */
    public double getPropertyAsDouble(String key, double defaultValue) {
        String s = getProperty(key);
        double d;
        try {
            d = Double.parseDouble(s);
        } catch (NullPointerException | NumberFormatException e) {
            d = defaultValue;
        }
        return d;
    }
    
    public void setProperty(String key, String value) {
        appSettings.setProperty(key, value);
    }
}
