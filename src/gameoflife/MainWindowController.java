package gameoflife;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;

public class MainWindowController implements Initializable
{
    private static final String KEY_SHOWINDICATORS = "s";
    private static final String KEY_INVERTGRID = "i";
    private static final String KEY_MINIGAME = "m";
    private static final String KEY_SHOWFIGURES = "f";
    private static final String KEY_EXPORT = "e";
    private static final String KEY_RESET = "r";
    private static final String KEY_NEXTSTEP = "n";
    private static final String KEY_MULTISTEP = "g";

    private static final String FIGUREWINDOW_RESNAME = "FigureWindow.fxml";

    private ResourceBundle langBundle;
    
    private GameOfLifeHandler gameHandler;
    private GameFileIo gameFileIo;
    private Timer gameTimer;
    private TimerTask gameTimerTask;

    private boolean miniGameMode;

    @FXML private AnchorPane mainPane;
    @FXML private Pane controlPane;
    @FXML private TextField colsTextfield;
    @FXML private TextField rowsTextfield;
    @FXML private Slider speedSlider;
    @FXML private ChoiceBox<String> scenarioChoicebox;
    @FXML private Label stepLabel;
    @FXML private Label aliveCellsLabel;
    @FXML private Pane minigamePane;
    @FXML private Label scoreLabel;
    @FXML private Label drawsLabel;
    @FXML private Label curCellPosLabel;

    @FXML private GameCanvasController gameCanvasPaneController;
    
    private FigureWindowController figureWindow;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        langBundle = resources;
        
        initializeFigureWindow();
        
        // initialize scenario save/loader
        try {
            gameFileIo = new GameFileIo();
        } catch (TransformerConfigurationException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }

        // Add hard coded scenarios as well as xml file scenarios to choice box
        final List<String> scenarioNames = GameFileIo.getAllScenarioFilenames();
        scenarioChoicebox.getItems().addAll(scenarioNames);

        // listeners which are not specified in fxml file are added here
        // Handle changed Scenario Choice
        scenarioChoicebox.getSelectionModel().selectedIndexProperty()
                         .addListener((obs, oldVal, newVal) -> {
                             scenarioChoiceChanged(newVal.intValue());
                         });

        // Handle changed simulation speed
        speedSlider.valueProperty().addListener((obs, oldVal, newVal) -> {
            speedSliderChanged();
        });

        gameTimer = new Timer();
        resetGrid();
    }

    private void resetGrid() {
        gameCanvasPaneController.reset();

        speedSlider.setValue(0); // stop simulation

        // create empty grid
        if (miniGameMode) {
            gameHandler = new MiniGameHandler();
            colsTextfield.setText(String.valueOf(gameHandler.getGridColumns()));
            rowsTextfield.setText(String.valueOf(gameHandler.getGridRows()));
        } else {
            final int cols = Integer.parseInt(colsTextfield.getText());
            final int rows = Integer.parseInt(rowsTextfield.getText());
            gameHandler = new GameOfLifeHandler(cols, rows);
        }
        gameCanvasPaneController.setGameHandlerReference(gameHandler);

        scenarioChoicebox.setValue(null);
        minigamePane.setVisible(miniGameMode);

        updateGameUi();
    }

    private void updateGameUi() {
        gameCanvasPaneController.updateGameCanvas();
        updateGameLabels();
    }

    private void updateGameLabels() {
        javafx.application.Platform.runLater(() -> {
            // update step count label
            stepLabel.setText(String.valueOf(gameHandler.getStepCount()));

            // update alive cell count label
            final int percentage = (int) (100.0 * gameHandler.getAliveCellCount() 
                / gameHandler.getGridSize());
            final String str = gameHandler.getAliveCellCount() + " (" + percentage + "%)";
            aliveCellsLabel.setText(str);

            // update mini game related labels
            if (miniGameMode) {
                scoreLabel.setText(String.valueOf(((MiniGameHandler) gameHandler).getScore()));
                drawsLabel.setText(String.valueOf(((MiniGameHandler) gameHandler).getDrawsLeft()));
            }

        });
    }
    
    private void doNextStep() {
        gameHandler.doStep();
        updateGameUi();
    }
    
    // If other window sends figure to this window
    // -> update currently selected figure
    public void receiveCellFigure(final CellFigure figure) {
        gameCanvasPaneController.receiveCellFigure(figure);
    }

    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------
    // UI Interaction/Events
    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------

    // handle key presses
    @FXML
    private void windowKeyPress(final KeyEvent event) {
        switch (event.getCharacter()) {
            case KEY_SHOWINDICATORS:
                gameCanvasPaneController.setShowCellLiveIndicators(
                    !gameCanvasPaneController.getShowCellLiveIndicators());
                updateGameUi();
                break;
            case KEY_INVERTGRID:
                gameHandler.invertGridCells();
                updateGameUi();
                break;
            case KEY_MINIGAME:
                miniGameMode = !miniGameMode;
                resetGrid();
                break;
            case KEY_SHOWFIGURES:
                showFigureWindow();
                break;
            case KEY_EXPORT:
                exportGrid();
                break;
            case KEY_RESET:
                resetGrid();
                break;
            case KEY_NEXTSTEP:
                doNextStep();
                break;
            case KEY_MULTISTEP:
                openMultiStepDialog();
                break;
            default:
                break;
        }
    }
    
    @FXML
    private void resetBtnClick() {
        resetGrid();
    }

    @FXML
    private void stepBtnClick() {
        doNextStep();
    }
    
    @FXML
    private void exportBtnClick() {
        exportGrid();
    }

    @FXML
    private void canvasMousePress(final MouseEvent event) {
        gameCanvasPaneController.onMousePress(event);
        updateGameLabels();
    }

    @FXML
    private void canvasMouseRelease(final MouseEvent event) {
        gameCanvasPaneController.onMouseRelease(event);
        updateGameLabels();
    }

    @FXML
    private void canvasMouseDrag(final MouseEvent event) {
        gameCanvasPaneController.onMouseDrag(event);
        updateGameLabels();
        updateCellPositionLabel(event.getX(), event.getY());
    }

    @FXML
    private void canvasMouseMove(final MouseEvent event) {
        updateCellPositionLabel(event.getX(), event.getY());
    }
    
    @FXML
    private void figureWindowBtnClick() {
        if (figureWindow != null) {
            showFigureWindow();
        }
    }

    @FXML
    private void speedSliderChanged() {
        if (gameTimerTask != null) {
            gameTimerTask.cancel();
        }

        final int speed = (int) speedSlider.getValue();
        if (speed > 0) {
            gameTimerTask = new TimerTask() {
                public void run() {
                    gameHandler.doStep();
                    updateGameUi();
                }
            };

            // set timer to run "speed" times per second
            gameTimer.scheduleAtFixedRate(gameTimerTask, 1000 / speed, 1000 / speed);
        }
    }

    private void updateCellPositionLabel(final double mouseX, final double mouseY) {
        final int curIndex = gameCanvasPaneController.getCellIndexByMousePos(mouseX, mouseY);
        javafx.application.Platform.runLater(() -> {
            // pad strings if necessary
            final String colIndex = String.format("%1$" + 2 + "s",
                                           String.valueOf(curIndex / gameHandler.getGridColumns()));
            final String rowIndex = String.format("%1$" + 2 + "s",
                                           String.valueOf(curIndex % gameHandler.getGridColumns()));
            curCellPosLabel.setText("( " + colIndex + " | " + rowIndex + " )");
        });
    }
    
    private void openMultiStepDialog() {
        speedSlider.setValue(0); // stop simulation
        Dialog<String> multiStepDialog = new TextInputDialog();
        multiStepDialog.setTitle(langBundle.getString("DoMultipleSteps"));
        multiStepDialog.setHeaderText(null);
        multiStepDialog.setContentText(langBundle.getString("DoMultipleSteps"));
        Platform.runLater(() -> {
            multiStepDialog.showAndWait();
            String s = multiStepDialog.getResult();
            int targetStep;
            try {
                targetStep = Integer.parseInt(s);
            } catch (Exception e) {
                targetStep = 0;
            }
            for (int i = 0; i < targetStep; ++i) {
                gameHandler.doStep();
            }
            updateGameUi();
        });
    }

    /**
     * Opens simply save dialog and returns user selected file.
     */
    private File openExportDialog(String initialDir) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(initialDir));
        fileChooser.getExtensionFilters()
                   .addAll(new FileChooser.ExtensionFilter(GameFileIo.FILE_EXT_NAME,
                                                           "*" + GameFileIo.FILE_EXT));
        return fileChooser.showSaveDialog(null);
    }
    
    /**
     * Stores grid as scenario or figure.
     * Opens dialog to let user choose save file
     */
    private void exportGrid() {
        // First check if suggest export as figure or scenario
        int cellIndex = 0;
        final int gridSize = gameHandler.getGridSize();
        for (; cellIndex < gridSize; ++cellIndex) {
            if (gameHandler.isCellAliveNow(cellIndex)) {
                break;
            }
        }
        String initialDir;
        // If found cell cluster has same number of cells as the whole board, it's probably a figure
        if (gameHandler.getConnectedCells(cellIndex).size() == gameHandler.getAliveCellCount()) {
            initialDir = GameFileIo.FIGURES_PATH;
        } else {
            initialDir = GameFileIo.SCENARIO_PATH;
        }
        
        // open save dialog to get file name
        final File file = openExportDialog(initialDir);
        if (file != null) {
            String name = file.getName();
            // Strip extension as it will be added later on anyway
            if (name.endsWith(GameFileIo.FILE_EXT)) {
                name = name.substring(0, name.lastIndexOf(GameFileIo.FILE_EXT));
            }

            // write grid to file
            // check if store in figure path
            // otherwise store as scenario in scenario path
            if (file.getAbsolutePath().contains(GameFileIo.FIGURES_PATH)) {
                gameFileIo.writeToFile(name, gameHandler.getGrid(), true);
                figureWindow.updateFigureListView();
            } else {
                gameFileIo.writeToFile(name, gameHandler.getGrid(), false);

                // add scenario to choice box
                if (!scenarioChoicebox.getItems().contains(name)) {
                    scenarioChoicebox.getItems().add(name);
                }
                scenarioChoicebox.setValue(name);
            }
        }
    }
    
    /**
     * set up figure window, but don't display yet.
     * stores reference in field figureWindow
     */
    private void initializeFigureWindow()
    {
        final FXMLLoader loader = 
            new FXMLLoader(getClass().getResource(FIGUREWINDOW_RESNAME), langBundle);
        
        try {
            Stage figureWindowStage = new Stage();
            figureWindowStage.setTitle(langBundle.getString("FigureWindowTitle"));
            figureWindowStage.setScene(new Scene(loader.load()));
            
            figureWindow = loader.getController();
            figureWindow.setMainWindow(this);
            figureWindow.setStage(figureWindowStage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * show figure window.
     */
    private void showFigureWindow() {
        figureWindow.updateFigureListView();
        
        Stage figureWindowStage = figureWindow.getStage();
        figureWindowStage.show();
        figureWindowStage.setIconified(false);
        // setting window position after .show() otherwise it does not work
        final Stage mainStage = (Stage) mainPane.getScene().getWindow();
        // place window left of mainwindow
        figureWindowStage.setX(Math.max(mainStage.getX() - figureWindowStage.getWidth(), 0));
        figureWindowStage.setY(mainStage.getY());
    }

    /**
     * handle value change of scenario choicebox.
     * @param newChoiceIndex current choice of choicebox
     */
    private void scenarioChoiceChanged(final int newChoiceIndex) {
        if (newChoiceIndex >= 0 && !miniGameMode) {
            // read grid from file
            gameHandler = gameFileIo.readScenarioFromFile(
                           scenarioChoicebox.getItems().get(newChoiceIndex));
            gameCanvasPaneController.setGameHandlerReference(gameHandler);

            colsTextfield.setText(String.valueOf(gameHandler.getGridColumns()));
            rowsTextfield.setText(String.valueOf(gameHandler.getGridRows()));

            updateGameUi();
        }
    }
}