package gameoflife;

import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Main extends Application
{
    private static final double minWindowWidth = 600;
    private static final double minWindowHeight = 400;
    
    /**
     * instance to read/write settings from/to file.
     */
    private SettingsIo appSettings = new SettingsIo();
    
    private Stage mainStage;
    
    @Override
    public void start(final Stage primaryStage) {
        mainStage = primaryStage;
        
        // Get display language -> Internationalization
        String appLanguage = appSettings.getProperty(SettingsIo.KEY_LANGUAGE);
        Locale loc;
        if (appLanguage != null) {
            loc = new Locale(appLanguage);
        } else {
            loc = Locale.getDefault();
            appSettings.setProperty(SettingsIo.KEY_LANGUAGE, loc.getLanguage());
        }
        ResourceBundle langBundle =
            ResourceBundle.getBundle("bundles.LangBundle", loc);
        
        FXMLLoader fxmlLoader = 
            new FXMLLoader(getClass().getResource("MainWindow.fxml"), langBundle);
        
        // Start MainWindow
        try {
            // Read file fxml and draw interface.
            final Parent root = fxmlLoader.load();

            primaryStage.setTitle(langBundle.getString("MainWindowTitle"));
            primaryStage.setScene(new Scene(root));
            primaryStage.setMinWidth(minWindowWidth);
            primaryStage.setMinHeight(minWindowHeight);
            
            Rectangle2D screenSize = Screen.getPrimary().getVisualBounds();

            primaryStage.setWidth(appSettings.getPropertyAsDouble(SettingsIo.KEY_WIDTH,
                                                                  screenSize.getWidth() / 2));
            primaryStage.setHeight(appSettings.getPropertyAsDouble(SettingsIo.KEY_HEIGHT, 
                                                                   screenSize.getHeight() / 2));
            primaryStage.setOpacity(appSettings.getPropertyAsDouble(SettingsIo.KEY_OPACITY, 1.0));
            
            primaryStage.show();
            primaryStage.centerOnScreen();
            
            // Terminate Application on MainWindow close
            primaryStage.setOnCloseRequest(event -> {
                this.stop(); // is not called otherwise
                Platform.exit();
                System.exit(0);
            });
            
            root.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void stop() {
        // On close, write updated properties to file
        appSettings.setProperty(SettingsIo.KEY_WIDTH, String.valueOf(mainStage.getWidth()));
        appSettings.setProperty(SettingsIo.KEY_HEIGHT, String.valueOf(mainStage.getHeight()));
        appSettings.setProperty(SettingsIo.KEY_OPACITY, String.valueOf(mainStage.getOpacity()));
        appSettings.writeToFile();
    }

    public static void main(final String[] args) {
        launch(args);
    }
}
