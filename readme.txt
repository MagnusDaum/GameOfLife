CONWAY'S GAME OF LIFE

Solution to code competition for September 2017 by IT-Talents.de
https://www.it-talents.de/foerderung/code-competition/code-competition-09-2017

This project is an implementation of "Conway's Game Of Life"
Written in Java using JavaFX as GUI framework 
Developed with JDK 8 and Windows 10 64bit

-------------------------------------------------------------------------------------

FEATURES

- Fully functional implementation of Conway's game of life
- Variable sized orthogonal 2D grid of cells (adjustable row and column count)
- Grid allows for wrap around on the edges (cells at left and right border are adjacent, as well as cells at top and bottom border)
- Steps can be done manually, or automatically via specifying the simulation speed
- Grid states can be im- and exported (as .xml file)
- A cluster of connected alive cells (=Figure) can be moved as well as im- and exported (as .xml file)
- A rudimentary minigame is available
- Several UI elements are provided in the statusbar showing the current state of the simulation
- Settings file for some basic settings (settings.cfg, language can be changed here)

-------------------------------------------------------------------------------------

HOW TO USE

Left click or moving the mouse while left mouse button is pressed will "revive" a cell.
Right click or moving the mouse while right mouse button is pressed will "kill" a cell.
Holding the middle mouse button while moving the mouse will move the selected cluster of alive cells
On the right, the number of rows and columns of the grid can be changed (will be applied after pressing "Reset")
"Reset" will clear the grid and set the dimensions to the specified number of rows and columns
"Next Step" will advance the simulation a single step
The "Speed"-Slider allows changing the simulation speed, i.e. how many steps should be simulated per second
A drop-down menu labelled "Scenarios" allows for selecting a preset grid state to be loaded
"Show Figures" will open a second window listing available figures to be inserted to the grid
	Selecting an entry will show a preview of the figure
	By pressing "Confirm" or double-clicking an entry, the figure is preliminarily added to the main grid and can be moved to the desired position
The "Export"-Button allows for exporting a full grid or a cell figure:
	On pressing, and file dialog opens
	Viable storing locations are ./Scenarios and ./Figures
	If directory "Scenarios" is selected, the grid is stored including the exact row and column count
	If directory "Figures" is selected, all alive cells of the grid are stored as a Figure and can subsequently be accessed via the figure window
By pressing "i", the state of all cells is switched
By pressing "s", small indicators are displayed which show what cells will be alive in the next step
By pressing "f", the figure window is shown
By pressing "e", the export dialog is shown
By pressing "r", the grid is reset
By pressing "n", the next step is simulated
By pressing "g", a dialog opens to simulate multiple steps at once
By pressing "m", the small minigame is started/stopped
	Objective of this game is to reach the highest score possible in 100 steps
	After every step, 1 point is awarded per alive cell
	Setting the state of a cell costs 1 action, of which the player has 200 at the beginning

-------------------------------------------------------------------------------------	