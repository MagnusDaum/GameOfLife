CONWAY'S GAME OF LIFE

Solution to code competition for September 2017 by IT-Talents.de
https://www.it-talents.de/foerderung/code-competition/code-competition-09-2017

This project is an implementation of "Conway's Game Of Life"
Written in Java using JavaFX as GUI framework 
Developed with JDK 8 and Windows 10 64bit

The submitted solution has the following structure:
/src/gameoflife - source code
/src/bundles - language files (english and german)
GameOfLife.jar - runnable jar file of the project
readme.txt - explains how to use the software (nothing about it's internal structure is explained)

In /src/ the following source files are present:
CellFigure.java 			- data structure to hold a cluster of alive cells
CellFigurePreset.java 		- preset hardcoded constellations of cells which can be added to the grid
FigureWindow.fxml 			- JavaFX GUI file, window to add cell figures to the grid
FigureWindowController.java - controller of FigureWindow.fxml
GameCanvas.fxml 			- JavaFX GUI file, handles the canvas on which the grid is drawn
GameCanvasController.java 	- controller of GameCanvas.fxml
GameFileIo.java 			- class to import/export a grid from/to .xml-file. Handles scenarios as well as figures
GameGrid.java 				- class to store the GameOfLife 2D grid with it's cells
GameOfLifeHandler.java 		- "main" class, holds the GameGrid and provides logic e.g. the ruleset how cells propagate
Main.java 					- starts the application window
MainWindow.fxml 			- JavaFX GUI file, "main" window, displayes grid and corresponding UI elements, handles keyboard input
MainWindowController.java 	- controller of MainWindow.fxml
MiniGameHandler.java 		- class inheriting from GameOfLifeHandler, extends some functionality to provide a simple mini-game
SettingsIo.java 			- class to load/store some basic settings (in settings.cfg plain text file)